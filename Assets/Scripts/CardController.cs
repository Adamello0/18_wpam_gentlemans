﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardController : MonoBehaviour
{
    //if any of cards on the board is moved other should stay calm no matter what
    static bool anyCardIsUp = false;

    //Punty, w których karty są zaczepione (Do których wracają)
    public Vector3 anchor;
    public float velocity;
    public float anchorMargin;
    public GameObject table;
    public GameObject pausePanel;

    public Canvas canvas;

    string cardText;

    System.Action<string,GameObject> releasedOnTableListener=null;

    BoxCollider2D cardCollider;

    int cardId{get; set;}

    

    float offsetX, offsetY;
    bool moveAllowed;
    public bool IsUp
    {
        get
        {
            return moveAllowed;
        }
    }

    // Use this for initialization
    void Start()
    {
        cardCollider = GetComponent<BoxCollider2D>();
        moveAllowed = false;
    }

    public void Initialize(Vector3 anchor,GameObject table, GameObject pausePanel, string text, int cardId,
        System.Action<string, GameObject> releasedOnTableListener,
        float velocity = 0.1f, float anchorMargin=0.1f)
    {
        this.anchor = anchor;
        this.velocity = velocity;
        this.anchorMargin = anchorMargin;
        this.table = table;
        this.pausePanel = pausePanel;

        //How high this card will be in hierachy?
        this.cardId = cardId;
        //GetComponent<SpriteRenderer>().sortingOrder = cardId;
        //GetComponent<Canvas>().sortingOrder = cardId;
        //transform.GetChild(0).SetSiblingIndex(transform.GetSiblingIndex());
        Debug.Log(text+":");
        Debug.Log(transform.GetSiblingIndex());
        //GetComponent<Canvas>().transform.SetSiblingIndex(2 * cardId + 1);
        GetComponent<SpriteRenderer>().sortingLayerName = "WhiteCard" + cardId.ToString();
        canvas.sortingLayerName = "WhiteCard" + cardId.ToString();

        Text t = GetComponentInChildren<Text>();
        t.text = text;
        cardText = text;

        this.releasedOnTableListener = releasedOnTableListener;
        //At the beginning of the game card should be placed on its anchor
    }
    
    public void slideAnchor(float deltaX)
    {
        anchor.x += deltaX;
    }
    public void slideCardAndAnchor(float deltaX)
    {
        transform.position = new Vector3(transform.position.x+deltaX, transform.position.y);
        anchor.x += deltaX;
    }

    // Update is called once per frame
    void Update()
    {
        //If pause panel is active freeze cards!
        if(pausePanel!=null)
            if (pausePanel.activeInHierarchy) return;
        
        //The object that is running script has Collider2D. Get it:
#if UNITY_EDITOR
       editorTouch();
#endif
#if UNITY_ANDROID || UNITY_IPHONE
       //androidTouch();
#endif
    }

    private void editorTouch()
    {
        if (cardCollider.OverlapPoint(table.transform.position))
        {
            anyCardIsUp = false;
            releasedOnTheTable();
        }

        //First check if there were any touches
        if (Input.GetMouseButton(0))
        {
            if (isOtherCardUp())
            {
                stepToAnchor();
                return;
            }

            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (cardCollider.OverlapPoint(pos))
            { 
                if(!moveAllowed && !anyCardIsUp)
                {
                    moveAllowed = true;
                    anyCardIsUp = true;
                }
                if (!moveAllowed) return;

                float nextX = pos.x;
                float nextY = pos.y;
                
                transform.position = new Vector2(nextX, nextY);
            }
            else
            {
                if(moveAllowed)
                {
                    moveAllowed = false;
                    anyCardIsUp = false;
                }
                stepToAnchor();
            }

        }
        else
        {
            if (moveAllowed)
            {
                moveAllowed = false;
                anyCardIsUp = false;
            }
            stepToAnchor();
        }
    }

    private void androidTouch()
    {
        //First check if there were any touches
        if (Input.touchCount > 0)
        {
            if (isOtherCardUp())
            {
                stepToAnchor();
                return;
            }

            //If someone is touching the screen, get that touch!
            Touch touch = Input.GetTouch(0);
            Vector3 pos = Camera.main.ScreenToWorldPoint(touch.position);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (cardCollider.OverlapPoint(pos))
                    {
                        offsetX = pos.x - transform.position.x;
                        offsetY = pos.y - transform.position.y;
                        moveAllowed = true;
                        anyCardIsUp = true;
                    }
                    else
                    {
                        //If the screen is beeing touched but the card is not
                        stepToAnchor();
                    }
                    break;
                case TouchPhase.Moved:
                    if (moveAllowed)
                    {
                        //Debug.Log("Oh! I was moved!", gameObject);
                        float nextX = pos.x - offsetX;
                        float nextY = pos.y - offsetY;

                        transform.position = new Vector2(nextX, nextY);
                    }
                    break;
                case TouchPhase.Ended:
                    //Debug.Log("Oh! I was released!", gameObject);
                    if (moveAllowed) anyCardIsUp = false;

                    moveAllowed = false;

                    //If it was released on the table - Than Event
                    if (cardCollider.OverlapPoint(table.transform.position))
                    {
                        releasedOnTheTable();
                    }

                    stepToAnchor();
                    break;
            }
        }
        else
        {
            stepToAnchor();
        }
    }
    private void stepToAnchor()
    {
        if(Vector3.Distance(transform.position,anchor)>anchorMargin)
            transform.position = velocity*(anchor - transform.position).normalized + transform.position;
        
    }

    private bool isOtherCardUp()
    {
        //If any card is up and it is not you
        if (anyCardIsUp && !moveAllowed)
            return true;
        return false;
    }

    private void releasedOnTheTable()
    {
        
        if(releasedOnTableListener!=null)
        {
            GameObject card = this.gameObject;
            releasedOnTableListener(cardText, card);
        }
    }
}


