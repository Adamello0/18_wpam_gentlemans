﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LobbyController : MonoBehaviour {
    public GameObject roomSelectorFile;
    public GameObject roomSelectorsContainer;

    public GameObject loginMenu;
    public GameObject lobby;

    public Text roomCreatorsRoomNameField;
    public Text nicknameField;

    MultiplayerController multiplayerController = MultiplayerGS.Instance;
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    public void refreshRoomsListener(List<GameRoom> gameRooms)
    {
        foreach (Transform child in roomSelectorsContainer.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (GameRoom room in gameRooms)
        {
            string id = room.id;

            //We have to instantiate in ON PARENT! Otherwise scale will be messed up!
            GameObject roomSel = Instantiate(roomSelectorFile, roomSelectorsContainer.transform);
            
            Button roomSelButton = roomSel.transform.Find("BtnChoose").gameObject.GetComponent<Button>();
            roomSelButton.onClick.AddListener(
                ()=> 
            {
                joinRoom(id);
            }
            );
            Text roomSelText = roomSel.transform.Find("TxtName").gameObject.GetComponent<Text>();
            roomSelText.text = room.name;
            //Text[] ttab = roomSel.GetComponentsInChildren<Text>();
            //ttab[0].text = room.name;
         
        }

        Debug.Log("Rooms where refreshed!");
    }
    public void refreshRoomList()
    {
        multiplayerController.getAllRooms(refreshRoomsListener);
    }

    public void createNewRoomListener(string roomId)
    {
        //What to do if room was created?
        Debug.Log("Room was created! id: " + roomId);

        Debug.Log("Now i'll try to join it");
        joinRoom(roomId);
    }
    public void createNewRoom()
    {
        string roomname = roomCreatorsRoomNameField.text;
        if (roomname == "")
            roomname = "No name";
        multiplayerController.createRoom(createNewRoomListener, roomname);
    }

    public void joinedRoomListener(bool ok)
    {
        if (ok)
        {
            Debug.Log("Joined the room!");
            SceneManager.LoadScene("Game");
        }
    }

    public void joinRoom(string roomId)
    {
        multiplayerController.joinRoom(joinedRoomListener, roomId);
    }

    public void loginListener(bool ok)
    {
        //If we logged in, we can refresh room list and go to lobby
        if (ok)
        {
            goToLobby();
        }
    }
    public void loginToGame()
    {
        Debug.Log("START MULTIPLAYER!!!");

        multiplayerController.loginAs(loginListener,nicknameField.text);
        //SceneManager.LoadScene("Game");
    }
    void goToLobby()
    {
        loginMenu.SetActive(false);
        lobby.SetActive(true);
        refreshRoomList();
    }

    //If player is not in any room, return ""; else returns room ID in which player is
    string getPlayersCurrentGame()
    {
        return multiplayerController.getCurrentRommId();
    }
}
