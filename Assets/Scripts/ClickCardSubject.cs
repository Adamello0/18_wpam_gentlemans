﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ClickCardSubject : MonoBehaviour, IPointerClickHandler
{
    SpriteRenderer rend;
    bool state = false;
    public bool Clickable { get; set; }
    string cardText;
    System.Action<string> winnerChooseListener;


    public string getCardText()
    {
        return cardText;
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!Clickable) return;
        setColGrey();
        winnerChooseListener(cardText);
    }
    
    public void Initialize(string cardText, System.Action<string> winnerChooseListener)
    {
        rend = GetComponent<SpriteRenderer>();
        Clickable = true;

        this.cardText = cardText;
        this.gameObject.GetComponentInChildren<Text>().text = cardText;
        this.winnerChooseListener = winnerChooseListener;
    }

    // Use this for initialization
    void Start () {
        rend = GetComponent<SpriteRenderer>();
        Clickable = true;
	}
	/*
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
        editorClick();
#endif
#if UNITY_ANDROID || UNITY_IPHONE
        //androidClick();
#endif
    }

    void androidClick()
    {
        if (Input.touchCount > 0)
        {
            //If someone is touching the screen, get that touch!
            Touch touch = Input.GetTouch(0);
            Vector3 pos = Camera.main.ScreenToWorldPoint(touch.position);

            if (touch.phase== TouchPhase.Began)
            {
                if (!Clickable) return;
                setColGrey();
                winnerChooseListener(cardText);
            }
        }
    }

    void editorClick()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (GetComponent<BoxCollider2D>().OverlapPoint(pos))
            {
                if (!Clickable) return;
                setColGrey();
                winnerChooseListener(cardText);
            }
        }
    }
    */

    public void setColGrey()
    {
        rend.color = new Color(0.7f, 0.7f, 0.7f);
    }
    public void setColWhite()
    {
        rend.color = new Color(1,1,1);
    }
    
}
