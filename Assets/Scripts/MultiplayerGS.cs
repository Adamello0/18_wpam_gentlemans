﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Api.Messages;
using GameSparks.Core;
using System;

public class MultiplayerGS : MultiplayerController
{
    private static MultiplayerGS _instance = null;

    System.Action<bool> loggedInListener;
    System.Action<string> createdRoomListener;
    string nameToSet;

    string currentRoomId="";
    string playerId = "";

    bool roomJustCreatedByPlayer=false;
    public bool isRoomJustCreatedByPlayer()
    {
        if(roomJustCreatedByPlayer)
        {
            roomJustCreatedByPlayer = false;
            return true;
        }
        return false;
    }
    //string lastJoinedRoomId=null;


    private MultiplayerGS()
    {
        // Code to initialize this object would go here
        setMessageListener();
    }

    public static MultiplayerGS Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new MultiplayerGS();
            }
            return _instance;
        }
    }


    public void deviceAuthenticated(AuthenticationResponse response)
    {
        bool ok=true;
        if (!response.HasErrors)
        {
            /*
            Debug.Log("Device Authenticated as:");
            Debug.Log(response.NewPlayer);
            Debug.Log(response.AuthToken);
            Debug.Log(response.DisplayName);
            Debug.Log(response.UserId);
            */
            playerId = response.UserId;
        }
        else
        {
            Debug.Log("Error Authenticating Device...");
            ok = false;
        }
        
        new ChangeUserDetailsRequest()
        .SetDisplayName(nameToSet)
        .Send(
            (dn_response) =>
            {
                if (!dn_response.HasErrors)
                {
                    //Debug.Log("User name changed");
                }
                else
                {
                    Debug.Log("Can't change username");
                }
            });

        loggedInListener(ok);
    }
    //Device authentication
    public void loginAs(System.Action<bool> loggedInListener, string name)
    {
        this.loggedInListener = loggedInListener;
        if(name == "")
        {
            name = "noname";
        }
        this.nameToSet = name;
        
        new DeviceAuthenticationRequest()
            //.SetDisplayName(name) is not used, becouse user device might have another (or NULL) name binded
            .Send(deviceAuthenticated) ;
    }
    public void getAllRooms(System.Action<List<GameRoom>> gotRoomsListener)
    {
        //First lets find challenges in which player does not challenge
        new FindChallengeRequest()
            .SetAccessType("PUBLIC")
            .Send(
            (findChallengeResponse) =>
            {
                List<GameRoom> gameRooms = new List<GameRoom>();

                if (findChallengeResponse.HasErrors)
                {
                    Debug.Log("Some errors occured while getting challanges list!");
                }
                foreach (var challenge in findChallengeResponse.ChallengeInstances)
                {
                    string name = challenge.ChallengeMessage;
                    string id = challenge.ChallengeId;
                    string state = challenge.State;
                    //if(state=="ISSUED"||state=="WAITING"||state=="RUNNING")
                    gameRooms.Add(new GameRoom(name, id));
                }



                //Now challenges in which player challenges
                List<string> states = new List<string>();
                states.Add("RUNNING");
                states.Add("WAITING");
                states.Add("ISSUED");

                new ListChallengeRequest()
                    .SetStates(states)
                    .Send(
                    (response) =>
                    {
                        if (response.HasErrors)
                        {
                            Debug.Log("There were some errors, while searching for challenged challenges");
                        }
                        else
                        {
                            foreach (var challenge in response.ChallengeInstances)
                            {
                                gameRooms.Add(new GameRoom(challenge.ChallengeMessage, challenge.ChallengeId));
                            }
                            gotRoomsListener(gameRooms);
                        }
                    }
                    );
            }
            );
    }
    public void roomCreated(CreateChallengeResponse createChallengeResponse)
    {
        string id = "-1";

        if (createChallengeResponse.HasErrors)
        {
            Debug.Log("Could not create room");
            Debug.Log(createChallengeResponse.Errors.JSON);
        }
        else
        {
            id = createChallengeResponse.ChallengeInstanceId;

            //Needed while init game in GameController (on scene change)
            roomJustCreatedByPlayer = true;
        }

        createdRoomListener(id);
    }
    //returns room id or -1 if error
    public void createRoom(System.Action<string> createdRoomListener,string name)
    {
        this.createdRoomListener = createdRoomListener;

        System.DateTime tomorrow = System.DateTime.Today;
        tomorrow = tomorrow.AddDays(1);
        new CreateChallengeRequest()
            .SetAccessType("PUBLIC")
            .SetChallengeShortCode("C")
            .SetChallengeMessage(name)
            .SetEndTime(tomorrow)
            .Send(roomCreated);
    }
    public void joinRoom(System.Action<bool> joinedRoomListener, string roomId)
    {
        new JoinChallengeRequest()
            .SetChallengeInstanceId(roomId)
            .Send(
            (joinChallengeResponse)
            =>
            {
                bool ok = true;
                if (joinChallengeResponse.HasErrors)
                {
                    Debug.Log("Could not join a challenge:");
                    Debug.Log(joinChallengeResponse.JSONString);
                    ok = false;
                }
                else
                {
                    this.currentRoomId = roomId;
                }
                joinedRoomListener(ok);
            }
            );
    }

    public void leaveCurrentRoom(Action<bool> leftRoomListener)
    {
        if(currentRoomId=="")
        {
            Debug.Log("No current room");
            leftRoomListener(false);
            return;
        }

        new LogEventRequest()
        .SetEventKey("LEAVE")
        .SetEventAttribute("CID", currentRoomId)
        .Send((response) => {
            //GSData scriptData = response.ScriptData;
            if(response.HasErrors)
            {
                Debug.Log("Could not leave the current room");
                leftRoomListener(false);
            }
            else
            {
                currentRoomId = "";
                leftRoomListener(true);
            }
        });

    }

    public string getCurrentRommId()
    {
        return currentRoomId;
    }

    public void genChooseWinningCard(Action<bool> genChooseWinningCardListener, string card)
    {

        if (currentRoomId == "")
        {
            Debug.Log("No current room");
            genChooseWinningCardListener(false);
            return;
        }

        new LogEventRequest()
       .SetEventKey("GEN_CHOOSE_CARD")
       .SetEventAttribute("CID", currentRoomId)
       .SetEventAttribute("WC", card)
       .Send((response) => {
            //GSData scriptData = response.ScriptData;
            if (response.HasErrors)
           {
               Debug.Log("Could not choose card as gentleman");
               genChooseWinningCardListener(false);
           }
           else
           {
               genChooseWinningCardListener(true);
           }
       });
    }

    public void plMakesMove(Action<bool> plMakeMoveListener, string card)
    {
        if (currentRoomId == "")
        {
            Debug.Log("No current room");
            plMakeMoveListener(false);
            return;
        }
        Debug.Log("WYSYŁAM KARTĘ: " + card);

        new LogEventRequest()
       .SetEventKey("PLAYER_PUSH_CARD")
       .SetEventAttribute("CID", currentRoomId)
       .SetEventAttribute("WC", card)
       .Send((response) => {
           //GSData scriptData = response.ScriptData;
           if (response.HasErrors)
           {
               Debug.Log("Could not make a move as a player");
               plMakeMoveListener(false);
           }
           else
           {
               plMakeMoveListener(true);
           }
       });
    }

    public void msgRoomState()
    {
        if (currentRoomId == "")
        {
            Debug.Log("No current room");
            return;
        }
        new LogEventRequest()
       .SetEventKey("GET_CHALLENGE_STATE")
       .SetEventAttribute("CID", currentRoomId)
       .Send((response) => {
           //GSData scriptData = response.ScriptData;
           if (response.HasErrors)
           {
               Debug.Log("Could not send message to get game state");
           }
       });


    }


    Action<GameState> newRoundListener=null;
    Action<GameState> justJoinedNewRoundListener = null;
    Action<int> newCardOnTableListener=null;
    Action<List<string>> everyoneIsReadyListener = null;

    public void setMessagesListeners(Action<GameState> newRoundListener, Action<GameState>  justJoinedNewRoundListener,
                                     Action<int> newCardOnTableListener, Action<List<string>> everyoneIsReadyListener)
    {
        this.newRoundListener= newRoundListener;
        this.newCardOnTableListener= newCardOnTableListener;
        this.everyoneIsReadyListener= everyoneIsReadyListener;
        this.justJoinedNewRoundListener = justJoinedNewRoundListener;
    }

    void setMessageListener()
    {

        GameSparks.Api.Messages.ScriptMessage.Listener += messageListener;
    }

    public void messageListener(ScriptMessage message)
    {
        if(newRoundListener==null || newCardOnTableListener==null || everyoneIsReadyListener ==null || justJoinedNewRoundListener==null)
        {
            //It's ok for as for the first msg
            //Debug.Log("Can't listen message from server! Functions in MultiplayerGS not initialized");

            return;
        }
        //myObject = JsonUtility.FromJson<MyClass>(json);
        /*
         Spark.sendMessageExt(
            {"WhiteCards" : Object.keys(tablewc)},
            "EveryoneIsReady", 
            sparkPlayer);    
         */
        if (message.ExtCode == "EveryoneIsReady")
        {
            //everyoneIsReadyListener();
            List<string> whiteCards = message.Data.GetStringList("WhiteCards");

            everyoneIsReadyListener(whiteCards);
        }
        /*
         {
                    "NewGentleNick" : gentleNick,
                    "PlayersNicknames" : nicks,
                    "PlayersPoints" : points,
                    "LastWhiteCards" : lastRoundTablewc,
                    "WinningCard" : winningCard,
                    "NewBlackCard" : newTablebc,
                    "MyHand" : hand,
                    "AmIGentleman" : isGentle
         }
         */
        if (message.ExtCode == "NewRound")
        {

            GameState gs = new GameState(
                message.Data.GetString("NewGentleNick"),
                message.Data.GetStringList("PlayersNicknames"),
                message.Data.GetIntList("PlayersPoints"),
                message.Data.GetStringList("LastWhiteCards"),
                message.Data.GetString("WinningCard"),
                message.Data.GetString("NewBlackCard"),
                message.Data.GetStringList("MyHand"),
                message.Data.GetBoolean("AmIGentleman").Value);

            newRoundListener(gs);
        }
        /*
         {"count" : cotCount},
            "CardsOnTableCount", 
         */
        if (message.ExtCode == "CardsOnTableCount")
        {
            int count = message.Data.GetInt("count").Value;

            newCardOnTableListener(count);
        }
        /*
         {
            "NewGentleNick" : gentleNick,
            "PlayersNicknames" : nicks,
            "PlayersPoints" : points,
            "NewBlackCard" : newTablebc,
            "MyHand" : hand,
            "AmIGentleman" : isGentle,
            "CardsCount" : ...
        },
        "GameState", 
         */
        if (message.ExtCode == "GameState")
        {
            GameState gs = new GameState(
                message.Data.GetString("NewGentleNick"),
                message.Data.GetStringList("PlayersNicknames"),
                message.Data.GetIntList("PlayersPoints"),
                null,
                null,
                message.Data.GetString("NewBlackCard"),
                message.Data.GetStringList("MyHand"),
                message.Data.GetBoolean("AmIGentleman").Value);

            newRoundListener(gs);
        }
    }
}
