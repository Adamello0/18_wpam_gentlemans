﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class GameRoom
{
    public GameRoom(string name, string id)
    {
        this.name = name;
        this.id = id;
    }
    public string id;
    public string name;
}

public interface MultiplayerController {
    void loginAs(System.Action<bool> loggedInListener,string name);
    void getAllRooms(System.Action<List<GameRoom>> gotRoomsListener);
    void createRoom(System.Action<string> createdRoomListener,string name);
    void joinRoom(System.Action<bool> joinedRoomListener, string roomId);

    void leaveCurrentRoom(System.Action<bool> leftRoomListener);
    string getCurrentRommId();

    void genChooseWinningCard(System.Action<bool> genChooseWinningCardListener,string card);
    void plMakesMove(System.Action<bool> plMakeMoveListener,string card);
    void msgRoomState();

    bool isRoomJustCreatedByPlayer();

    void setMessagesListeners(Action<GameState> newRoundListener, Action<GameState> justJoinedNewRoundListener,
                                     Action<int> newCardOnTableListener, Action<List<string>> everyoneIsReadyListener);
}
