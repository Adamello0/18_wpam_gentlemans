﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameState {
    public string gentlemanNickname;
    public List<string> playersNicknames;
    public List<int> playersPoints;
    public List<string> lastWhiteCards;
    public string lastWinningCard;
    public string newBlackCard;
    public List<string> playerHand;
    public bool isPlayerGentleman;
    public int cardsOnTableCount;

    public GameState()
    {
        this.gentlemanNickname = null;
        this.playersNicknames = null;
        this.playersPoints = null;
        this.lastWhiteCards = null;
        this.lastWinningCard = null;
        this.newBlackCard = null;
        this.playerHand = null;
        this.isPlayerGentleman = false;
        this.cardsOnTableCount = 0;
    }

    public GameState(string gentlemanNickname, List<string> playersNicknames, List<int> playersPoints,
                     List<string> lastWhiteCards, string lastWinningCard, string newBlackCard, 
                     List<string> playerHand, bool isPlayerGentleman, int cardsOnTableCount = 0)
    {
        this.gentlemanNickname= gentlemanNickname;
        this.playersNicknames= playersNicknames;
        this.playersPoints= playersPoints;
        this.lastWhiteCards= lastWhiteCards;
        this.lastWinningCard= lastWinningCard;
        this.newBlackCard = newBlackCard;
        this.playerHand= playerHand;
        this.isPlayerGentleman= isPlayerGentleman;
        this.cardsOnTableCount= cardsOnTableCount;
    }

    public string asString()
    {
        string s = "";
        s += "Gentleman: " + gentlemanNickname + "\n";
        s += "Players/points: ";
        int count = playersNicknames.Count;
        for (int i= 0; i < count; ++i)
        {
            s += playersNicknames[i] + "/" + playersPoints[i] + "; ";
        }
        s += "\n";
        s += "Last white cards: ";
        foreach(string wc in lastWhiteCards)
        {
            s += " " + wc;
        }
        s += "\n";
        s += "Last winning card: " + lastWinningCard + "\nNew black card: " + newBlackCard + "\n";
        s += "Player hand: ";
        foreach (string card in playerHand)
        {
            s += card + " ";
        }
        s += "\n";
        s += "Is player gentleman: " + isPlayerGentleman.ToString() + "\n" + 
            "Cards count on table: " + cardsOnTableCount.ToString();

        return s;
    }
}
