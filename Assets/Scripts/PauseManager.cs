﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour {
    MultiplayerController multiplayerController = MultiplayerGS.Instance;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PauseGame()
    {
    }
    public void ResumeGame()
    {
    }
    public void EndGame()
    {
        //End game and go back to menu
        multiplayerController.leaveCurrentRoom(
            (ok)
            =>
            {
                if(!ok)
                {
                    Debug.Log("Could not leave current room!");
                }
                SceneManager.LoadScene("Menu");
            }
            );
    }
}
