﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandSlider : MonoBehaviour {
    public GameObject pausePanel;

    float handBottom;
    float handTop;
    List<GameObject> cards;
    float handWidth;

    bool isSwiping;
    float lastXPos;

    // Use this for initialization
    void Start ()
    {
        isSwiping = false;
    }
    public void Initialize(float cardsHeight, float handBottom, float handWidth, List<GameObject> cards)
    {
        Debug.Log(cardsHeight);
        handTop = handBottom + cardsHeight;
        this.handBottom = handBottom;
        this.cards = cards;
        this.cards = cards;
        this.handWidth = handWidth;
    }

    // Update is called once per frame
    void Update ()
    {
        //If pause panel is active freeze cards!
        if (pausePanel != null)
            if (pausePanel.activeInHierarchy) return;

#if UNITY_EDITOR
        editorTouch();
#endif
#if UNITY_ANDROID || UNITY_IPHONE
        //androidTouch();
#endif
    }


    private void editorTouch()
    {
        //First check if there were any touches
        if (Input.GetMouseButton(0))
        {
            Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //If the touch happend on the hand field
            if (pos.y < handTop && pos.y > handBottom)
            {
                if (isSwiping)
                {
                    float deltaX = pos.x - lastXPos;
                    moveAllCardsInHand(deltaX);
                    lastXPos = pos.x;
                }
                else
                {
                    isSwiping = true;
                    lastXPos = pos.x;
                }
            }
            else
            {
                isSwiping = false;
            }
        }
        else
        {
            isSwiping = false;
        }
    }

    private void androidTouch()
    {
        //First check if there were any touches
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector3 pos = Camera.main.ScreenToWorldPoint(touch.position);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (pos.y < handTop && pos.y > handBottom)
                    {
                        isSwiping = true;
                        lastXPos = pos.x;
                    }
                    else
                    {
                        isSwiping = false;
                    }
                    break;
                case TouchPhase.Moved:
                    if (pos.y < handTop && pos.y > handBottom)
                    {
                        if(!isSwiping)
                        {
                            isSwiping = true;
                            lastXPos = pos.x;
                            return;
                        }
                        float deltaX = pos.x - lastXPos;//Camera.main.ScreenToWorldPoint(touch.deltaPosition).x;//pos.x - lastXPos;
                        moveAllCardsInHand(deltaX);
                        lastXPos = pos.x;
                    }
                    else
                    {
                        isSwiping = false;
                    }
                    break;
                case TouchPhase.Ended:
                    isSwiping = false;
                    break;
            }
        }
        else
        {
            isSwiping = false;
        }
    }
    // Moves all cards in hand 
    // Checks if the hand is out of screen - than stops it
    void moveAllCardsInHand(float deltaX)
    {

        float xMin=0;
        float xMax=0;
        //Check where are bounds of hands
        foreach(GameObject card in cards)
        {
            CardController cc = card.GetComponent<CardController>();
            if (cc.anchor.x < xMin) xMin = cc.anchor.x;
            if (cc.anchor.x > xMax) xMax = cc.anchor.x;
        }
        // Do not move hand left if its right bound is <5f (bound of screen)
        if (deltaX < 0 //left 
            && xMax + deltaX < 5f) return;
        // ... rigiht ...
        if (deltaX > 0 //right 
            && xMin + deltaX > -5f) return;

        foreach (GameObject card in cards)
        {
            CardController cc= card.GetComponent<CardController>();
            if (!cc.IsUp)
                cc.slideCardAndAnchor(deltaX);
            else
                cc.slideAnchor(deltaX);
        }
    }
}
