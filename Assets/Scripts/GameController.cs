﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEditor;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Xml.Serialization;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    MultiplayerController multiplayerController = MultiplayerGS.Instance;

    public GameObject whiteCardPrefab;
    public GameObject blackCardPrefab;
    public GameObject clickCardPrefab;

    public GameObject tablePrefab;
    public GameObject handSlider;

    public GameObject txtLeaderboard;

    public Camera camera;

    public GameObject pausePanel;

    List<GameObject> cardsInHand;
    GameObject blackCard =null;

    GameObject table;

    int roundCounter = 0;

    bool canReleaseCardThisRound=true;

    GameState gameState;

    string blackCardOnTable;
    
    void Start ()
    {
        roundCounter = 0;

        //Runs every time we load the scene
        gameState = new GameState();

        cardsInHand = new List<GameObject>();

        //lockScreenRotationLandscape(); -already locked
        
        table = Instantiate(tablePrefab, new Vector3(0.0f, 0.0f), Quaternion.identity);

        //TODO:
        //Set listeners in multiplayerController
        multiplayerController.setMessagesListeners(newRoundListener, justJoinedNewRoundListener,
                                                    newCardOnTableListener, everyoneIsReadyListener);

        //Only if you have created room - skip this part. otherwise: get your cards!
        if (!multiplayerController.isRoomJustCreatedByPlayer())
        {
            multiplayerController.msgRoomState();
        }
    }
    void justJoinedNewRoundListener(GameState game)
    {
        Debug.Log("JUST JOINED: NEW ROUND STARTS NOW!");

        roundCounter++;

        gameState = game;

        if (game.isPlayerGentleman)
        {
            clearWhiteCards();
            canReleaseCardThisRound = false;
        }
        else
        {
            resetTable(gameState.playerHand);
            canReleaseCardThisRound = true;
        }

        resetBlackCard(gameState.newBlackCard);
        resetPointsInfo(gameState.playersNicknames, gameState.playersPoints);

        //Set the number of cards on the table to 0
        newCardOnTableListener(game.cardsOnTableCount);
    }

    void newRoundListener(GameState game)
    {
        gameState = game;

        roundCounter++;
        //Last round was not first round -> show white cards on the table and show winning card
        if (gameState.lastWinningCard!=null && gameState.lastWhiteCards.Count>0 && roundCounter>1)
        {
            showClickCards(gameState.lastWhiteCards, true, gameState.lastWinningCard);
        }

        

        if (gameState.isPlayerGentleman)
        {
            clearWhiteCards();
            canReleaseCardThisRound = false;
        }
        else
        {
            resetTable(gameState.playerHand);
            canReleaseCardThisRound = true;
        }
        
        resetBlackCard(gameState.newBlackCard);
        resetPointsInfo(gameState.playersNicknames, gameState.playersPoints);

        //Set the number of cards on the table to 0
        newCardOnTableListener(0);
    }
    void newCardOnTableListener(int count)
    {
        //Now change the number on the table:
        Text t = table.GetComponentInChildren<Text>();
        t.text = count.ToString();
    }


    List<GameObject> clickCards=null;
    List<GameObject> presentationClickCards=null;
    void everyoneIsReadyListener(List<string> cardsTexts)
    {
        Debug.Log("Oh! Everyone is ready!");

        //if the clickcards from last round are not deleted - delete them now
        if(presentationClickCards!=null)
        {
            clearPresentationClickCards(" ");
        }
        
        showClickCards(cardsTexts);
        //Now wait untill player clicks
    }

    void showClickCards(List<string> cardsTexts, bool onlyPresentation=false, string winner=null)
    {
        Debug.Log("Ill show you click cards");

        List<GameObject> cc;
        if (onlyPresentation)
        {
            if (presentationClickCards != null) return;

            presentationClickCards = new List<GameObject>();
            cc = presentationClickCards;
        }
        else
        {
            if (clickCards != null) return;

            clickCards = new List<GameObject>();
            cc = clickCards;
        }

        float cardWidth = 2 * clickCardPrefab.GetComponent<Transform>().lossyScale.x;
        float cardHeight = 2 * clickCardPrefab.GetComponent<Transform>().lossyScale.y;
        float gap = cardWidth / 5;
        

        //Only one row
        if (cardsTexts.Count <= 3)
        {
            int id = 0;
            float totalWidth = (cardsTexts.Count - 1f) * (cardWidth + gap);
            float deltaX = cardWidth+gap;
            for (float x = -totalWidth / 2; x <= totalWidth / 2 + 0.0001f; x = x + deltaX)
            {
                Vector3 pos = new Vector3(x, 0);
                GameObject card = Instantiate(clickCardPrefab, pos, Quaternion.identity);
                
                if (onlyPresentation)
                {
                    Debug.Log("Showing card in presentation mode");
                    card.GetComponent<ClickCardSubject>().Initialize(cardsTexts[id], clearPresentationClickCards);
                    if (card.GetComponent<ClickCardSubject>().getCardText() == winner)
                    {
                        card.GetComponent<ClickCardSubject>().setColGrey();
                    }
                }
                else
                {
                    Debug.Log("Showing card in gamemode");
                    card.GetComponent<ClickCardSubject>().Initialize(cardsTexts[id], winnerChooseListener);
                }
                    


                cc.Add(card);
                ++id;
            }
        }
        //2 rows
        else
        {
            int id = 0;
            int topCount = cardsTexts.Count / 2;
            int botCount = cardsTexts.Count - topCount;

            float totalWidth = (topCount - 1f) * (cardWidth + gap);
            float deltaX = cardWidth + gap;
            float y = (cardHeight)/2 + gap;
            for (float x = -totalWidth / 2; x <= totalWidth / 2 + 0.0001f; x = x + deltaX)
            {
                Vector3 pos = new Vector3(x, y);
                GameObject card = Instantiate(clickCardPrefab, pos, Quaternion.identity);
                
                if (onlyPresentation)
                {
                    card.GetComponent<ClickCardSubject>().Initialize(cardsTexts[id], clearPresentationClickCards);
                    if(card.GetComponent<ClickCardSubject>().getCardText() == winner)
                    {
                        card.GetComponent<ClickCardSubject>().setColGrey();
                    }
                }
                else
                    card.GetComponent<ClickCardSubject>().Initialize(cardsTexts[id], winnerChooseListener);

                cc.Add(card);
                ++id;
            }

            totalWidth = (botCount - 1f) * (cardWidth + gap);
            y = -y;
            for (float x = -totalWidth / 2; x <= totalWidth / 2 + 0.0001f; x = x + deltaX)
            {
                Vector3 pos = new Vector3(x, y);
                GameObject card = Instantiate(clickCardPrefab, pos, Quaternion.identity);
                
                if (onlyPresentation)
                {
                    card.GetComponent<ClickCardSubject>().Initialize(cardsTexts[id], clearPresentationClickCards);
                    if (card.GetComponent<ClickCardSubject>().getCardText() == winner)
                    {
                        card.GetComponent<ClickCardSubject>().setColGrey();
                    }
                }
                else
                    card.GetComponent<ClickCardSubject>().Initialize(cardsTexts[id], winnerChooseListener);

                cc.Add(card);
                ++id;
            }
        }
    }
    void clearPresentationClickCards(string clickedCard)
    {
        Debug.Log("Oh, card was clicked! Clear all clickcards!");
        foreach (GameObject card in presentationClickCards)
        {
            card.SetActive(false);
            Destroy(card);
        }
        presentationClickCards = null;
    }

    void winnerChooseListener(string winningCard)
    {
        
        foreach (GameObject card in clickCards)
        {
            Debug.Log("Before Error");
            card.GetComponent<ClickCardSubject>().Clickable = false;
            Debug.Log("After error");
        }
        
        /*
        foreach (GameObject card in clickCards)
        {
            Debug.Log("Killing a clickcard after winner choice!");
            card.SetActive(false);
            Destroy(card);
        }
        clickCards = null;
        */

        multiplayerController.genChooseWinningCard(
            (ok) =>
            {
                if(!ok)
                {
                    Debug.Log("Gentleman could not send information about card he choose");
                    
                    foreach (GameObject card in clickCards)
                    {
                        card.GetComponent<ClickCardSubject>().Clickable = true;
                        card.GetComponent<ClickCardSubject>().setColWhite();
                    }
                    
                }
                else
                {
                    Debug.Log("Destroying clickcards, after the winner has choosen one...");
                    
                    foreach (GameObject card in clickCards)
                    {
                        card.SetActive(false);
                        Destroy(card);
                    }
                    clickCards = null;
                }
            },
            winningCard);
    }
    
    void releasedOnTableListener(string cardText, GameObject card)
    {
        //If there are any "clickcards" on the table, don't let user release card. He have to click them first!
        if (presentationClickCards != null) return;

        if(canReleaseCardThisRound)
        {
            multiplayerController.plMakesMove(
                (ok) =>
                {
                    if(!ok)
                    {
                        canReleaseCardThisRound = true;
                    }
                    else
                    {
                        canReleaseCardThisRound = false;

                        //Make card disappear:
                        card.SetActive(false);
                    }
                }
                ,cardText);
        }
    }
    void clearWhiteCards()
    {
        foreach (GameObject card in cardsInHand)
        {
            Destroy(card);
        }
    }
    void resetTable(List<string> cardsInHandStrings)
    {
        clearWhiteCards();

        cardsInHand = new List<GameObject>();
        //wtf
        float cardWidth = 2 * whiteCardPrefab.GetComponent<Transform>().lossyScale.x;
        float gap = cardWidth / 5;

        int i = 0;
        int cardsCount = cardsInHandStrings.Count;

        foreach(string cardString in cardsInHandStrings)
        {
            Vector3 leftCardPos = new Vector3(-((cardsCount - 1f) * (cardWidth + gap)) / 2, -4f);
            Vector3 initPos = leftCardPos + new Vector3(i * (cardWidth + gap), 0f);

            GameObject wc = Instantiate(whiteCardPrefab, initPos, Quaternion.identity);
            wc.GetComponent<CardController>().Initialize(initPos, table, pausePanel, cardString, i, releasedOnTableListener);
            cardsInHand.Add(wc);

            ++i;
        }

        handSlider.GetComponent<HandSlider>().Initialize(
            whiteCardPrefab.GetComponent<BoxCollider2D>().size.y,
            -5f,
            5f,
            cardsInHand
            );
    }

    class PointsText
    {
        public PointsText(int points, string text)
        {
            this.Text = text;
            this.Points = points;
        }
        public string Text { get; set; }
        public int Points { get; set; }
    }
    void resetPointsInfo(List<string> players, List<int> points)
    {
        List<PointsText> pt = new List<PointsText>();
        for(int i=0; i<players.Count; ++i)
        {
            pt.Add(new PointsText(points[i], players[i] + ": " + points[i].ToString()));
        }
        var ordered = pt.OrderBy(var => var.Points);
        string t = "Wyniki: \n";
        foreach(PointsText tup in ordered)
        {
            t += tup.Text + "\n";
        }
        txtLeaderboard.GetComponentInChildren<Text>().text = t;
    }
    void resetBlackCard(string bc)
    {
        if (blackCard != null) Destroy(blackCard);
        Vector3 initPos = new Vector3(1.5f,3.5f);
        blackCard = Instantiate(blackCardPrefab, initPos, Quaternion.identity);
        blackCard.GetComponentInChildren<Text>().text = bc;
    }

    
    public void lockScreenRotationLandscape()
    {
#if UNITY_ANDROID || UNITY_IPHONE
        Screen.autorotateToPortrait = false;
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
#endif
    }
    public void unlockScreenRotation()
    {
#if UNITY_ANDROID || UNITY_IPHONE
        Screen.autorotateToPortrait = true;
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
#endif
    }
}